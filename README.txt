Couchbase cache backends
====================

This package provides Couchbase cache backend.

PhpCouchbase
--------

This implementation uses the PhpCouchbase PHP extention. In order to use it, you
probably will need to compile the extension yourself to get the latest version.

Couchbase version
-------------
Please use Couchbase 2.X or later if you can.

Notes
-----
Note that most of the settings are shared. See next sections.

Important notice
----------------

This module only supports Couchbase >= 2.X

Getting started
===============

Quick setup
-----------

Here is a simple yet working easy way to setup the module.
This method will Drupal to use Couchbase for all caches and path alias
cache replacement.

Make following updates in settings.php file:

  $conf['couchbase_client_interface'] = 'PhpCouchbase'; // couchbase php SDK.
  $conf['cache_backends'][]       = 'sites/all/modules/couchbase_cache/couchbase.autoload.inc';
  $conf['path_inc']               = 'sites/all/modules/couchbase_cache/couchbase.path.inc';
  $conf['cache_default_class']    = 'Couchbase_Cache';
  $conf['couchbase_client_host'] = '127.0.0.1'; // IP of your Couchbase server
  $conf['couchbase_client_port'] = 8091; // Defaults to 8091
  $conf['couchbase_client_bucket'] = 'default'; // bucket
  $conf['couchbase_client_username'] = 'admin'; // username
  $conf['couchbase_client_password'] = 'admin123'; // password

See next chapters for more information.

Is there any cache bins that should *never* go into Couchbase?
----------------------------------------------------------
The 'cache_form' bin must be assigned no non-volatile storage.


Couchbase has been maturing a lot over time, and will apply different sensible
settings for different bins; It's today very stable.

Advanced configuration
======================

Choose the Couchbase client library to use
--------------------------------------

Add into your settings.php file:

  $conf['couchbase_client_interface']      = 'PhpCouchbase';

Tell Drupal to use the cache backend
------------------------------------

Usual cache backend configuration, as follows, to add into your settings.php
file like any other backend:

  $conf['cache_backends'][]            = 'sites/all/modules/couchbase_cache/couchbase.autoload.inc';
  $conf['cache_class_cache']           = 'Couchbase_Cache';
  $conf['cache_class_cache_menu']      = 'Couchbase_Cache';
  $conf['cache_class_cache_bootstrap'] = 'Couchbase_Cache';
  // ... Any other bins.

Common settings
===============

Connect to a remote host
------------------------

If your Couchbase instance is remote, you can use this syntax:

  $conf['couchbase_client_host'] = '1.2.3.4';
  $conf['couchbase_client_port'] = 1234;
  $conf['couchbase_client_bucket'] = 'default'; // default bucket

Port is optional, default is 8091 (default Couchbase port).

Prefixing site cache entries (avoiding sites name collision)
------------------------------------------------------------

If you need to differenciate multiple sites using the same Couchbase instance
you will need to specify a prefix for your site cache entries.

Cache prefix configuration attemps to use a unified variable accross contrib
backends that support this feature. This variable name is 'cache_prefix'.

This variable is polymorphic, the simplest version is to provide a raw string
that will be the default prefix for all cache bins:

  $conf['cache_prefix'] = 'mysite_';

Alternatively, to provide the same functionality, you can provide the variable
as an array:

  $conf['cache_prefix']['default'] = 'mysite_';

This allows you to provide different prefix depending on the bin name. Common
usage is that each key inside the 'cache_prefix' array is a bin name, the value
the associated prefix. If the value is explicitely FALSE, then no prefix is
used for this bin.

The 'default' meta bin name is provided to define the default prefix for non
specified bins. It behaves like the other names, which means that an explicit
FALSE will order the backend not to provide any prefix for any non specified
bin.

Here is a complex sample:

  // Default behavior for all bins, prefix is 'mysite_'.
  $conf['cache_prefix']['default'] = 'mysite_';

  // Set no prefix explicitely for 'cache' and 'cache_bootstrap' bins.
  $conf['cache_prefix']['cache'] = FALSE;
  $conf['cache_prefix']['cache_bootstrap'] = FALSE;

  // Set another prefix for 'cache_menu' bin.
  $conf['cache_prefix']['cache_menu'] = 'menumysite_';

Note that if you don't specify the default behavior, the Couchbase module will
attempt to use the a hash of the database credentials in order to provide a
multisite safe default behavior. Notice that this is not failsafe. In such
environments you are strongly advised to set at least an explicit default
prefix.

Note that this last notice is Couchbase only specific, because per default
Couchbase server will not namespace data, thus sharing an instance for multiple
sites will create conflicts. This is not true for every contributed backends.

Flush mode
----------
If you don't want to flush a specific bin use the following pattern in settings.
We have two different implementations of the flush algorithm you can use:

 * 1: (Default Mode) Flush everything including permanent or valid items on
      clear() calls.

 * 0: Never Flush Complete Cache Bin: If you do not want to flush all keys of
      'cache_page' bin.
      // Restrict "cache_page" bin to be flushed all
      $conf['couchbase_flush_mode_cache_page'] = 0;

To override couchbase timeout variable use following settings:

$conf['couchbase_timeout'] = 5000000; // Time in microseconds


Note that you must prefix your bins with "cache" as the Drupal 7 bin naming
convention requires it.

Keep in mind that defaults will provide the best balance between performance
and safety for most sites; Non advanced users should ever change them.