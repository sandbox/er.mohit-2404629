<?php

/**
 * PhpCouchbase client specific implementation.
 */
class Couchbase_Client_PhpCouchbase implements Couchbase_Client_Interface {

  public function getClient($host = '127.0.0.1', $port = 8091, $user = NULL, $password = NULL, $bucket = 'default') {
    global $conf;
    $client = NULL;
    try {
      $client = new Couchbase("$host:$port", "$user", "$password", "$bucket");
      if (!empty($conf['couchbase_timeout'])) {
        $timeout = (int) $conf['couchbase_timeout'];
        $client->setTimeout($timeout);
      }
    } catch (Exception $e) {
      watchdog("couchbase", $client->getResultMessage(), array(), WATCHDOG_ERROR);
      $client = NULL;
    }
    return $client;
  }

  public function getName() {
    return 'PhpCouchbase';
  }

}
