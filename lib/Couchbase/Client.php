<?php

// It may happen we get here with no autoloader set during the Drupal core
// early bootstrap phase, at cache backend init time.
if (!interface_exists('Couchbase_Client_Interface')) {
  require_once dirname(__FILE__) . '/Client/Interface.php';
}

/**
 * Common code and client singleton, for all Couchbase clients.
 */
class Couchbase_Client {
  /**
   * Couchbase default host.
   */

  const COUCHBASE_DEFAULT_HOST = "127.0.0.1";

  /**
   * Couchbase default port.
   */
  const COUCHBASE_DEFAULT_PORT = 8091;

  /**
   * Couchbase default bucket.
   */
  const COUCHBASE_DEFAULT_BUCKET = 'default';

  /**
   * Couchbase default username.
   */
  const COUCHBASE_DEFAULT_USERNAME = NULL;

  /**
   * Couchbase default password.
   */
  const COUCHBASE_DEFAULT_PASSWORD = NULL;

  /**
   * Cache implementation namespace.
   */
  const COUCHBASE_IMPL_CACHE = 'Couchbase_Cache_';

  /**
   * Lock implementation namespace.
   */
  const COUCHBASE_IMPL_LOCK = 'Couchbase_Lock_Backend_';

  /**
   * Session implementation namespace.
   */
  const COUCHBASE_IMPL_SESSION = 'Couchbase_Session_Backend_';

  /**
   * Path implementation namespace.
   */
  const COUCHBASE_IMPL_PATH = 'Couchbase_Path_';

  /**
   * Client implementation namespace.
   */
  const COUCHBASE_IMPL_CLIENT = 'Couchbase_Client_';

  /**
   * @var Couchbase_Client_Interface
   */
  protected static $_clientInterface;

  /**
   * @var mixed
   */
  protected static $_client;

  /**
   * @var string
   */
  protected static $globalPrefix;

  /**
   * Has this instance a client set.
   *
   * @return boolean
   */
  public static function hasClient() {
    return isset(self::$_client);
  }

  /**
   * Set client proxy.
   */
  public static function setClient(Couchbase_Client_Interface $interface) {
    if (isset(self::$_client)) {
      throw new Exception("Once Couchbase client is connected, you cannot change client proxy instance.");
    }

    self::$_clientInterface = $interface;
  }

  /**
   * Lazy instanciate client proxy depending on the actual configuration.
   *
   * If you are using a lock, session or cache backend using one of the Couchbase
   * client implementation, this will be overrided at early bootstrap phase
   * and configuration will be ignored.
   *
   * @return Couchbase_Client_Interface
   */
  public static function getClientInterface() {
    global $conf;

    if (!isset(self::$_clientInterface)) {
      if (!empty($conf['couchbase_client_interface'])) {
        $className = self::getClass(self::COUCHBASE_IMPL_CLIENT, $conf['couchbase_client_interface']);
        self::$_clientInterface = new $className();
      } else if (class_exists('Couchbase')) {
        // Fallback on PhpCouchbase if available.
        $className = self::getClass(self::COUCHBASE_IMPL_CLIENT, 'PhpCouchbase');
        self::$_clientInterface = new $className();
      } else {
        if (!isset(self::$_clientInterface)) {
          throw new Exception("No client interface set.");
        }
      }
    }

    return self::$_clientInterface;
  }

  /**
   * Get underlaying library name.
   *
   * @return string
   */
  public static function getClientName() {
    return self::getClientInterface()->getName();
  }

  /**
   * Get client singleton.
   */
  public static function getClient() {
    if (!isset(self::$_client)) {
      global $conf;

      // Always prefer socket connection.
      self::$_client = self::getClientInterface()->getClient(
        isset($conf['couchbase_client_host']) ? $conf['couchbase_client_host'] : self::COUCHBASE_DEFAULT_HOST, isset($conf['couchbase_client_port']) ? $conf['couchbase_client_port'] : self::COUCHBASE_DEFAULT_PORT, isset($conf['couchbase_client_username']) ? $conf['couchbase_client_username'] : self::COUCHBASE_DEFAULT_USERNAME, isset($conf['couchbase_client_password']) ? $conf['couchbase_client_password'] : self::COUCHBASE_DEFAULT_PASSWORD, isset($conf['couchbase_client_bucket']) ? $conf['couchbase_client_bucket'] : self::COUCHBASE_DEFAULT_BUCKET);
    }

    return self::$_client;
  }

  /**
   * Get specific class implementing the current client usage for the specific
   * asked core subsystem.
   *
   * @param string $system
   *   One of the Couchbase_Client::IMPL_* constant.
   * @param string $clientName
   *   Client name, if fixed.
   *
   * @return string
   *   Class name, if found.
   *
   * @throws Exception
   *   If not found.
   */
  public static function getClass($system, $clientName = NULL) {
    $className = $system . (isset($clientName) ? $clientName : self::getClientName());

    if (!class_exists($className)) {
      throw new Exception($className . " does not exists");
    }

    return $className;
  }

  /**
   * For unit testing only reset internals.
   */
  static public function reset() {
    self::$_clientInterface = null;
    self::$_client = null;
  }

}

