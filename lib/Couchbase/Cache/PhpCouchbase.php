<?php

/**
 * Phpcouchbase cache backend.
 * http://www.couchbase.com/autodocs/couchbase-php-client-1.1.5/classes/Couchbase.html .
 */
class Couchbase_Cache_PhpCouchbase extends Couchbase_Cache_Base {

  function get($cid) {
    $cached = FALSE;
    $client = NULL;
    try {
      $client = Couchbase_Client::getClient();
    } catch (Exception $e) {
      watchdog("couchbase", $client->getResultMessage(), array(), WATCHDOG_ERROR);
    }

    if ($client) {
      $key = $this->getKey($cid);

      switch ($this->getDataType()) {
        case Couchbase_Cache_Base::DATA_JSON:
          try {
            $cached = json_decode($client->get($key), true);
          } catch (Exception $e) {
            watchdog("couchbase", $client->getResultMessage(), array(), WATCHDOG_ERROR);
          }

          if (empty($cached) || !is_array($cached)) {
            return FALSE;
          }
          $cached = (object) $cached;
          if ($cached->serialized) {
            $cached->data = (object) $cached->data;
          }
          break;
        case Couchbase_Cache_Base::DATA_BINARY:
        default :
          try {
            $cached = $client->get($key);
          } catch (Exception $e) {
            watchdog("couchbase", $client->getResultMessage(), array(), WATCHDOG_ERROR);
          }

          if (empty($cached) || !is_array($cached)) {
            return FALSE;
          }
          $cached = (object) $cached;
          if ($cached->serialized) {
            $cached->data = unserialize($cached->data);
          }
          break;
      }
    }
    return $cached;
  }

  function getMultiple(&$cids) {
    $client = NULL;
    try {
      $client = Couchbase_Client::getClient();
    } catch (Exception $e) {
      watchdog("couchbase", $client->getResultMessage(), array(), WATCHDOG_ERROR);
    }

    $ret = array();
    if ($client) {
      $keys = array_map(array($this, 'getKey'), $cids);
      try {
        $replies = $client->getMulti($keys);
      } catch (Exception $e) {
        watchdog("couchbase", $client->getResultMessage(), array(), WATCHDOG_ERROR);
      }

      $dataType = $this->getDataType();
      if ($replies) {
        foreach ($replies as $reply) {
          if (!empty($reply)) {

            switch ($dataType) {
              case Couchbase_Cache_Base::DATA_JSON:
                $cached = (object) json_decode($reply, true);
                if ($cached->serialized) {
                  $cached->data = (object) $cached->data;
                }
                break;
              case Couchbase_Cache_Base::DATA_BINARY:
              default :
                $cached = (object) $reply;
                if ($cached->serialized) {
                  $cached->data = unserialize($cached->data);
                }
                break;
            }
            $ret[$cached->cid] = $cached;
          }
        }
      }

      foreach ($cids as $index => $cid) {
        if (isset($ret[$cid])) {
          unset($cids[$index]);
        }
      }
    }
    return $ret;
  }

  function set($cid, $data, $expire = CACHE_PERMANENT) {
    $client = NULL;
    try {
      $client = Couchbase_Client::getClient();
    } catch (Exception $e) {
      watchdog("couchbase", $client->getResultMessage(), array(), WATCHDOG_ERROR);
    }

    if ($client) {
      $key = $this->getKey($cid);
      // making permanent cache for cache_page bin.
      $expire = ($expire === CACHE_TEMPORARY) ? CACHE_PERMANENT : $expire;
      $hash = array(
        'cid' => $cid,
        'created' => time(),
        'expire' => $expire,
      );

      switch ($this->getDataType()) {
        case Couchbase_Cache_Base::DATA_JSON:
          if (!is_string($data)) {
            $hash['data'] = $data;
            $hash['serialized'] = 1;
          } else {
            $hash['data'] = $data;
            $hash['serialized'] = 0;
          }
          $hash = json_encode($hash);
          break;
        case Couchbase_Cache_Base::DATA_BINARY:
        default :
          // Let Couchbase handle the data types itself.
          if (!is_string($data)) {
            $hash['data'] = serialize($data);
            $hash['serialized'] = 1;
          } else {
            $hash['data'] = $data;
            $hash['serialized'] = 0;
          }
          break;
      }
      try {
        $client->set($key, $hash, $expire);
      } catch (Exception $e) {
        watchdog("couchbase", $client->getResultMessage(), array(), WATCHDOG_ERROR);
      }
    }
  }

  /**
   * Expires data from the cache.
   *
   * If called without arguments, expirable entries will be cleared from the
   * cache_page and cache_block bins.
   *
   * @param $cid
   *   If set, the cache ID or an array of cache IDs. Otherwise, all cache
   *   entries that can expire are deleted. The $wildcard argument will be
   *   ignored if set to NULL.
   * @param $wildcard
   *   If TRUE, the $cid argument must contain a string value and cache IDs
   *   starting with $cid are deleted in addition to the exact cache ID
   *   specified by $cid. If $wildcard is TRUE and $cid is '*', the entire
   *   cache is emptied.
   */
  function clear($cid = NULL, $wildcard = FALSE) {
    // Prepend cache prefix with view defination, since we may use one bucket
    // for multiple drupal installation.
    global $conf;
    $cache_prefix = (!empty($conf['cache_prefix']['default'])) ? $conf['cache_prefix']['default'] : NULL;
    $client = NULL;
    try {
      $client = Couchbase_Client::getClient();
    } catch (Exception $e) {
      watchdog("couchbase", $client->getResultMessage(), array(), WATCHDOG_ERROR);
    }
    if ($client) {
      $bin = $this->bin;
      $viewname = (!empty($cache_prefix)) ? $cache_prefix . "_" . $bin : $bin;
      $ret = NULL;
      $rows = NULL;
      $designdoc = (!empty($cache_prefix)) ? $cache_prefix . "_" . $bin . "_design" : $bin . "_design";

      // Create the map function.
      $func = "function (doc, meta) {if(meta.id.match(/" . $cache_prefix . Couchbase_AbstractBackend::KEY_SEPARATOR . $bin . Couchbase_AbstractBackend::KEY_SEPARATOR . "/i))emit(meta.id, null);}";

      // We cannot determine which keys are going to expire, so we need to flush
      // the full bin case we have an explicit NULL provided. This means that
      // stuff like block and cache pages may be expired too often.
      if (NULL === $cid) {
        switch ($this->getClearMode()) {

          // One and only case of early return.
          case Couchbase_Cache_Base::FLUSH_NOTHING:
            // Try to retrieve the desgin document.
            try {
              $ddoc = $client->getDesignDoc($designdoc);
              if (!$ddoc) {
                // Create document containing the map function.
                $ddoc = json_encode('{"views":{"' . $viewname .
                  '":{"map":"' . $func . '"}}}');
                // Create the design document on the server.
                $ret = $client->setDesignDoc($designdoc, json_decode($ddoc));
              }
            } catch (Exception $e) {
              watchdog("couchbase", $client->getResultMessage(), array(), WATCHDOG_ERROR);
            }

            return;

          // Fallback on most secure mode: flush full bin.
          default:
          case Couchbase_Cache_Base::FLUSH_ALL:
            // Full bin flush.
            try {
              switch ($bin) {
                case 'cache_page':
                case 'cache_block':
                  // Try to retrieve the desgin document.
                  $ddoc = $client->getDesignDoc($designdoc);
                  if (!$ddoc) {
                    // Create document containing the map function.
                    $ddoc = json_encode('{"views":{"' . $viewname .
                      '":{"map":"' . $func . '"}}}');
                    // Create the design document on the server.
                    $ret = $client->setDesignDoc($designdoc, json_decode($ddoc));
                    if ($ret) {
                      // Delay for 5 sec, first time view is created.
                      sleep(5);
                      $rows = $client->view($designdoc, $viewname);
                    }
                  } else {
                    // Get keys through view.
                    $rows = $client->view($designdoc, $viewname, array("stale" => "false"));
                  }
                  if (!empty($rows['rows'])) {
                    foreach ($rows['rows'] as $key => $row) {
                      if (!empty($row['key']))
                        try {
                          $client->delete($row['key']);
                        } catch (Exception $e) {
                          watchdog("couchbase", $client->getResultMessage(), array(), WATCHDOG_ERROR);
                        }
                    }
                  }
                  break;
              }
            } catch (Exception $e) {
              watchdog("couchbase", $client->getResultMessage(), array(), WATCHDOG_ERROR);
            }
            break;
        }
      } else if ('*' !== $cid && $wildcard) {
        try {
          // Prefix flush.
          // Try to retrieve the desgin document.
          $ddoc = $client->getDesignDoc($designdoc);
          if (!$ddoc) {
            // Create document containing the map function.
            $ddoc = json_encode('{"views":{"' . $viewname .
              '":{"map":"' . $func . '"}}}');
            // Create the design document on the server.
            $ret = $client->setDesignDoc($designdoc, json_decode($ddoc));
            if ($ret) {
              // Delay for 5 sec, first time view is created.
              sleep(5);
              $rows = $client->view($designdoc, $viewname);
            }
          } else {
            // Get keys through view.
            $rows = $client->view($designdoc, $viewname, array("stale" => "false"));
          }
          if (!empty($rows['rows'])) {
            foreach ($rows['rows'] as $key => $row) {
              if ((!empty($row['key'])) && (strpos($row['key'], $cid) !== false))
                try {
                  $client->delete($row['key']);
                } catch (Exception $e) {
                  watchdog("couchbase", $client->getResultMessage(), array(), WATCHDOG_ERROR);
                }
            }
          }
        } catch (Exception $e) {
          watchdog("couchbase", $client->getResultMessage(), array(), WATCHDOG_ERROR);
        }
      } else if ('*' === $cid && $wildcard) {
        // Full bin flush.
        switch ($this->getClearMode()) {
          // One and only case of early return.
          case Couchbase_Cache_Base::FLUSH_NOTHING:
            return;

          // Fallback on most secure mode: flush full bin.
          default:
          case Couchbase_Cache_Base::FLUSH_ALL:
            try {
              // Full bin flush.
              // Try to retrieve the desgin document.
              $ddoc = $client->getDesignDoc($designdoc);
              if (!$ddoc) {
                // Create document containing the map function.
                $ddoc = json_encode('{"views":{"' . $viewname .
                  '":{"map":"' . $func . '"}}}');
                // Create the design document on the server.
                $ret = $client->setDesignDoc($designdoc, json_decode($ddoc));
                if ($ret) {
                  // Delay for 5 sec, first time view is created.
                  sleep(5);
                  $rows = $client->view($designdoc, $viewname);
                }
              } else {
                // Get keys through view.
                $rows = $client->view($designdoc, $viewname, array("stale" => "false"));
              }
              if (!empty($rows['rows'])) {
                foreach ($rows['rows'] as $key => $row) {
                  if (!empty($row['key']))
                    try {
                      $client->delete($row['key']);
                    } catch (Exception $e) {
                      watchdog("couchbase", $client->getResultMessage(), array(), WATCHDOG_ERROR);
                    }
                }
              }
            } catch (Exception $e) {
              watchdog("couchbase", $client->getResultMessage(), array(), WATCHDOG_ERROR);
            }
            break;
        }
      } else if (!empty($cid)) {
        if (is_array($cid)) {
          foreach ($cid as $id) {
            $key = $this->getKey($id);
            try {
              $client->delete($key);
            } catch (Exception $e) {
              watchdog("couchbase", $client->getResultMessage(), array(), WATCHDOG_ERROR);
            }
          }
        } else {
          // Single key drop.
          $key = $this->getKey($cid);
          try {
            $client->delete($key);
          } catch (Exception $e) {
            watchdog("couchbase", $client->getResultMessage(), array(), WATCHDOG_ERROR);
          }
        }
      }
    }
  }

  function isEmpty() {
    global $conf;
    $cache_prefix = (!empty($conf['cache_prefix']['default'])) ? $conf['cache_prefix']['default'] : NULL;
    $client = Couchbase_Client::getClient();
    $bin = $this->bin;
    $viewname = (!empty($cache_prefix)) ? $cache_prefix . "_" . $bin : $bin;
    $rows = FALSE;
    $designdoc = (!empty($cache_prefix)) ? $cache_prefix . "_" . $bin . "_design" : $bin . "_design";

    // Create the map function.
    $func = "function (doc, meta) {if(meta.id.match(/" . $cache_prefix . Couchbase_AbstractBackend::KEY_SEPARATOR . $bin . Couchbase_AbstractBackend::KEY_SEPARATOR . "/i))emit(meta.id, null);}";

    // Try to retrieve the desgin document.
    $ddoc = $client->getDesignDoc($designdoc);
    if (!$ddoc) {
      // Create document containing the map function.
      $ddoc = json_encode('{"views":{"' . $viewname .
        '":{"map":"' . $func . '"}}}');
      // Create the design document on the server.
      $ret = $client->setDesignDoc($designdoc, json_decode($ddoc));
      if ($ret) {
        // Delay for 5 sec, first time view is created.
        sleep(5);
        $rows = $client->view($designdoc, $viewname);
      }
    } else {
      // Get keys through view.
      $rows = $client->view($designdoc, $viewname, array("stale" => "false"));
    }
    return empty($rows);
  }

}
