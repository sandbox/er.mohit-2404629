<?php

/**
 * PhpCouchbase implementation.
 *
 * @todo
 *   Set high expire value to the hash for rotation when memory is empty
 *   React upon cache clear all and rebuild path list?
 */
class Couchbase_Path_PhpCouchbase extends Couchbase_Path_AbstractHashLookup {

  protected function saveInHash($key, $hkey, $hvalue) {
    $client = NULL;
    try {
      $client = Couchbase_Client::getClient();
    } catch (Exception $e) {
      watchdog("couchbase", $client->getResultMessage(), array(), WATCHDOG_ERROR);
    }

    if ($client) {
      $combined_key = $key . self::KEY_SEPARATOR . $hkey;
      try {
        $value = json_decode($client->get($combined_key), true);
      } catch (Exception $e) {
        watchdog("couchbase", $client->getResultMessage(), array(), WATCHDOG_ERROR);
      }
      if ($value === self::VALUE_NULL) { // Remove any null values
        $value = null;
      }
      if ($value) {
        $existing = explode(self::VALUE_SEPARATOR, $value);
        if (!in_array($hvalue, $existing)) {
          // Prepend the most recent path to ensure it always be
          // first fetched one
          // @todo Ensure in case of update that its position does
          // not changes (pid ordering in Drupal core)
          $value = $hvalue . self::VALUE_SEPARATOR . $value;
        } else { // Do nothing on empty value
          $value = null;
        }
      } else if (empty($hvalue)) {
        $value = self::VALUE_NULL;
      } else {
        $value = $hvalue;
      }

      if (!empty($value)) {
        $value = json_encode($value);
        try {
          $client->set($combined_key, $value);
        } catch (Exception $e) {
          watchdog("couchbase", $client->getResultMessage(), array(), WATCHDOG_ERROR);
        }
      }
      // Empty value here means that we already got it
    }
  }

  public function saveAlias($source, $alias, $language = null) {
    if (null === $language) {
      $language = LANGUAGE_NONE;
    }

    if (!empty($source)) {
      $this->saveInHash($this->getKey(self::KEY_ALIAS, $language), $source, $alias);
    }
    if (!empty($alias)) {
      $this->saveInHash($this->getKey(self::KEY_SOURCE, $language), $alias, $source);
    }
  }

  protected function deleteInHash($key, $hkey, $hvalue) {
    $client = NULL;
    try {
      $client = Couchbase_Client::getClient();
    } catch (Exception $e) {
      watchdog("couchbase", $client->getResultMessage(), array(), WATCHDOG_ERROR);
    }
    if ($client) {
      $combined_key = $key . self::KEY_SEPARATOR . $hkey;
      try {
        $value = json_decode($client->get($combined_key), true);
      } catch (Exception $e) {
        watchdog("couchbase", $client->getResultMessage(), array(), WATCHDOG_ERROR);
      }
      if ($value) {
        $existing = explode(self::VALUE_SEPARATOR, $value);
        $index = array_search($hvalue, $existing);
        if ($index !== FALSE) {
          if (1 === count($existing)) {
            try {
              $client->delete($combined_key);
            } catch (Exception $e) {
              watchdog("couchbase", $client->getResultMessage(), array(), WATCHDOG_ERROR);
            }
          } else {
            unset($existing[$index]);
            $val = json_encode(implode(self::VALUE_SEPARATOR, $existing));
            try {
              $client->set($combined_key, $val);
            } catch (Exception $e) {
              watchdog("couchbase", $client->getResultMessage(), array(), WATCHDOG_ERROR);
            }
          }
        }
      }
    }
  }

  public function deleteAlias($source, $alias, $language = null) {
    if (null === $language) {
      $language = LANGUAGE_NONE;
    }

    $this->deleteInHash($this->getKey(self::KEY_ALIAS, $language), $source, $alias);
    $this->deleteInHash($this->getKey(self::KEY_SOURCE, $language), $alias, $source);
  }

  public function deleteLanguage($language) {
    try {
      $client = Couchbase_Client::getClient();
      $client->delete($this->getKey(self::KEY_ALIAS, $language));
      $client->delete($this->getKey(self::KEY_SOURCE, $language));
    } catch (Exception $e) {
      watchdog("couchbase", $client->getResultMessage(), array(), WATCHDOG_ERROR);
    }
  }

  public function lookupInHash($keyPrefix, $hkey, $language = null) {
    $client = NULL;
    $existing = NULL;
    try {
      $client = Couchbase_Client::getClient();
    } catch (Exception $e) {
      watchdog("couchbase", $client->getResultMessage(), array(), WATCHDOG_ERROR);
    }

    if ($client) {
      if (null === $language) {
        $language = LANGUAGE_NONE;
        $doNoneLookup = false;
      } else if (LANGUAGE_NONE === $language) {
        $doNoneLookup = false;
      } else {
        $doNoneLookup = true;
      }

      $ret = NULL;
      try {
        $ret = json_decode($client->get($this->getKey($keyPrefix, $language, $hkey)), true);
      } catch (Exception $e) {
        watchdog("couchbase", $client->getResultMessage(), array(), WATCHDOG_ERROR);
      }
      if ($doNoneLookup && (!$ret || self::VALUE_NULL === $ret)) {
        $previous = $ret;
        try {
          $ret = json_decode($client->get($this->getKey($keyPrefix, LANGUAGE_NONE, $hkey)), true);
        } catch (Exception $e) {
          watchdog("couchbase", $client->getResultMessage(), array(), WATCHDOG_ERROR);
        }
        if (!$ret && $previous) {
          // Restore null placeholder else we loose conversion to false
          // and drupal_lookup_path() would attempt saving it once again
          $ret = $previous;
        }
      }

      if (self::VALUE_NULL === $ret) {
        return false; // Needs conversion
      }
      if (empty($ret)) {
        return null; // Value not found
      }

      $existing = explode(self::VALUE_SEPARATOR, $ret);
    }
    return reset($existing);
  }

  public function lookupAlias($source, $language = null) {
    return $this->lookupInHash(self::KEY_ALIAS, $source, $language);
  }

  public function lookupSource($alias, $language = null) {
    return $this->lookupInHash(self::KEY_SOURCE, $alias, $language);
  }

}
