<?php

/**
 * Cache backend for Couchbase module.
 */
class Couchbase_Cache implements DrupalCacheInterface {

  /**
   * @var DrupalCacheInterface
   */
  protected $backend;
  public $bin;

  function __construct($bin) {
    $className = Couchbase_Client::getClass(Couchbase_Client::COUCHBASE_IMPL_CACHE);
    $this->backend = new $className($bin);
    $this->bin = $bin;
  }

  function get($cid) {
    $collect_stats = couchbase_stats_init();
    $result = $this->backend->get($cid);
    if ($collect_stats) {
      couchbase_stats_write('get', $this->bin, array($cid => (bool) $result));
    }
    return $result;
  }

  function getMultiple(&$cids) {
    $cids = array_filter($cids);
    if (!empty($cids)) {
      $collect_stats = couchbase_stats_init();
      $result = $this->backend->getMultiple($cids);
      $key_cids = implode(" | ", array_keys($result));
      if ($collect_stats) {
        if (!empty($key_cids)) {
          couchbase_stats_write('getMulti', $this->bin, array($key_cids => (bool) $result));
        } else {
          couchbase_stats_write('getMulti', $this->bin, array(implode(" | ", $cids) => FALSE));
        }
      }
      return $result;
    }
  }

  function set($cid, $data, $expire = CACHE_PERMANENT) {
    $collect_stats = couchbase_stats_init();
    $result = $this->backend->set($cid, $data, $expire);
    if ($collect_stats) {
      couchbase_stats_write('set', $this->bin, array($cid => (bool) $result));
    }
  }

  function clear($cid = NULL, $wildcard = FALSE) {
    // This function also accepts arrays, thus handle everything like an array.
    $cids = is_array($cid) ? $cid : array($cid);
    foreach ($cids as $cid) {
      $collect_stats = couchbase_stats_init();
      $result = $this->backend->clear($cid, $wildcard);
      if ($collect_stats) {
        couchbase_stats_write('clear', $this->bin, array($cid => (bool) $result));
      }
    }
  }

  function isEmpty() {
    return $this->backend->isEmpty();
  }

}

/**
 * Collect statistics if enabled.
 *
 * Optimized function to determine whether or not we should be collecting
 * statistics. Also starts a timer to track how long individual couchbase
 * operations take.
 *
 * @return bool
 *   TRUE or FALSE if statistics should be collected.
 */
function couchbase_stats_init() {
  static $drupal_static_fast;

  if (!isset($drupal_static_fast)) {
    $drupal_static_fast = &drupal_static(__FUNCTION__, array('variable_checked' => NULL, 'user_access_checked' => NULL));
  }
  $variable_checked = &$drupal_static_fast['variable_checked'];
  $user_access_checked = &$drupal_static_fast['user_access_checked'];

  // Confirm DRUPAL_BOOTSTRAP_VARIABLES has been reached. We don't use
  // drupal_get_bootstrap_phase() as it's buggy. We can use variable_get() here
  // because _drupal_bootstrap_variables() includes module.inc immediately
  // after it calls variable_initialize().
  if (!isset($variable_checked) && function_exists('module_list')) {
    $variable_checked = variable_get('show_couchbase_statistics', FALSE);
  }
  // If statistics are enabled we need to check user access.
  if (!empty($variable_checked) && !isset($user_access_checked) && !empty($GLOBALS['user']) && function_exists('user_access')) {
    // Statistics are enabled and the $user object has been populated, so check
    // that the user has access to view them.
    $user_access_checked = user_access('access couchbase statistics');
  }
  // Return whether or not statistics are enabled and the user can access them.
  if ((!isset($variable_checked) || $variable_checked) && (!isset($user_access_checked) || $user_access_checked)) {
    timer_start('couchbase');
    return TRUE;
  } else {
    return FALSE;
  }
}

/**
 * Save couchbase statistics to be displayed at end of page generation.
 *
 * @param string $action
 *   The action being performed (get, set, etc...).
 * @param string $bin
 *   The couchbase bin the action is being performed in.
 * @param array $keys
 *   Keyed array in the form (string)$cid => (bool)$success. The keys the
 *   action is being performed on, and whether or not it was a success.
 */
function couchbase_stats_write($action, $bin, $keys) {
  global $_couchbase_stats;
  // Determine how much time elapsed to execute this action.
  $time = timer_read('couchbase');
  // Build the 'all' and 'ops' arrays displayed
  foreach ($keys as $key => $success) {
    $_couchbase_stats['all'][] = array(
      number_format($time, 2),
      $action,
      $bin,
      $key,
      $success ? 'hit' : 'miss',
    );
    if (!isset($_couchbase_stats['ops'][$action])) {
      $_couchbase_stats['ops'][$action] = array($action, 0, 0, 0);
    }
    $_couchbase_stats['ops'][$action][1] += $time;
    if ($success) {
      $_couchbase_stats['ops'][$action][2]++;
    } else {
      $_couchbase_stats['ops'][$action][3]++;
    }
  }
  // Reset the couchbase timer for timing the next couchbase operation.
}
