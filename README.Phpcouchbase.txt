PhpCouchbase cache backend
======================

This client, for now, is only able to use the PhpCouchbase extension.

Get PhpCouchbase
------------

You can download this library at:

  https://github.com/couchbase/php-ext-couchbase

This is PHP extension, too recent for being packaged in most distribution, you
will probably need to compile it yourself.

Connect to a remote host
-------------------------------------

See README.txt file.