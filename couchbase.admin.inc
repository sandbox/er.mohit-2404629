<?php

/**
 * @file
 * Couchcache module administration pages.
 */

/**
 * Main settings and review administration screen.
 */
function couchbase_settings_form($form, &$form_state) {

  $form['connection'] = array(
    '#type' => 'fieldset',
    '#title' => t("Connection information"),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['connection']['couchbase_client_host'] = array(
    '#type' => 'textfield',
    '#title' => t("Host"),
    '#default_value' => variable_get('couchbase_client_host', NULL),
    '#description' => t("Couchbase server host. Default is <em>@default</em>.", array('@default' => Couchbase_Client::COUCHBASE_DEFAULT_HOST)),
  );
  $form['connection']['couchbase_client_port'] = array(
    '#type' => 'textfield',
    '#title' => t("Port"),
    '#default_value' => variable_get('couchbase_client_port', NULL),
    '#description' => t("Couchbase server port. Default is <em>@default</em>.", array('@default' => Couchbase_Client::COUCHBASE_DEFAULT_PORT)),
  );
  $form['connection']['couchbase_client_username'] = array(
    '#type' => 'textfield',
    '#title' => t("Username"),
    '#default_value' => variable_get('couchbase_client_username', NULL),
    '#description' => t("Couchbase server username. Default is none."),
  );
  $form['connection']['couchbase_client_password'] = array(
    '#type' => 'textfield',
    '#title' => t("Password"),
    '#default_value' => variable_get('couchbase_client_password', NULL),
    '#description' => t("Couchbase server password. Default is none."),
  );
  $form['connection']['couchbase_client_bucket'] = array(
    '#type' => 'textfield',
    '#title' => t("Bucket"),
    '#default_value' => variable_get('couchbase_client_bucket', NULL),
    '#description' => t("Couchbase server bucket. Default is <em>@default</em>.", array('@default' => Couchbase_Client::COUCHBASE_DEFAULT_BUCKET)),
  );
  $form['connection']['couchbase_client_interface'] = array(
    '#type' => 'radios',
    '#title' => t("Client"),
    '#options' => array(
      'PhpCouchbase' => t("PhpCouchbase PHP extension"),
    ),
    '#default_value' => variable_get('couchbase_client_interface', 'PhpCouchbase'),
    '#description' => t("Couchbase low level backend."),
  );
  $form['show_couchbase_statistics'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show couchbase statistics at the bottom of each page'),
    '#default_value' => variable_get('show_couchbase_statistics', FALSE),
    '#description' => t("These statistics will be visible to users with the 'access couchbase statistics' permission."),
  );

  $form = system_settings_form($form);

  // Enforce empty values drop from the $form_state in order to avoid empty
  // values saving. Empty values would cause the isset() checks in client
  // options to see false positives and fail upon connection.
  array_unshift($form['#submit'], 'couchbase_settings_form_submit_clean_values');

  return $form;
}

/**
 * Deep clean of $form_state values.
 */
function couchbase_settings_form_submit_clean_values($form, &$form_state) {

  $string_values = array('couchbase_client_host', 'couchbase_client_username', 'couchbase_client_password', 'couchbase_client_bucket', 'couchbase_client_interface');

  foreach ($string_values as $name) {
    // Empty check is sufficient to verify that the field is indeed empty.
    if (empty($form_state['values'][$name])) {
      // Using unset() will keep the key in the array, with an associated NULL
      // value. While this wouldn't really matter, it's safer to remove it so
      // that system_settings_form_submit() won't find it and attempt to save
      // it.
      $form_state['values'] = array_diff_key($form_state['values'], array($name => NULL));
      variable_del($name);
    }
  }

  $numeric_values = array('couchbase_client_port');

  foreach ($numeric_values as $name) {
    // Numeric values can be both of NULL or 0 (NULL meaning the value is not
    // not set and the client will use the default, while 0 has a business
    // meaning and should be kept as is).
    if ('0' !== $form_state['values'][$name] && empty($form_state['values'][$name])) {
      $form_state['values'] = array_diff_key($form_state['values'], array($name => NULL));
      variable_del($name);
    } else {
      $form_state['values'][$name] = (int) $form_state['values'][$name];
    }
  }
}
