<?php

/**
 * @file
 * Couchbase module autoloader.
 */
// Use the Autoload Early module if present.
if (class_exists('AutoloadEarly')) {
  AutoloadEarly::getInstance()->registerNamespace('Couchbase', dirname(__FILE__) . '/lib');
}
// Else, we have to register our own autoloader.
else {

  /**
   * Autoloader micro optimization, work with constant as much as we can.
   */
  define('COUCHBASE_ROOT', dirname(__FILE__) . '/lib');

  /**
   * Couchbase module specific autoloader, compatible with spl_register_autoload().
   */
  function couchbase_autoload($class_name) {
    $parts = explode('_', $class_name);

    if ('Couchbase' === $parts[0]) {
      $filename = COUCHBASE_ROOT . '/' . implode('/', $parts) . '.php';

      if (file_exists($filename)) {
        require_once $filename;
        return TRUE;
      }
    }

    return FALSE;
  }

  // Register our custom autoloader.
  spl_autoload_register('couchbase_autoload');
}